CREATE TABLE ProdLevel (
Code_level       char(12) PRIMARY KEY NOT NULL,	
Class_level      char(12) NOT NULL,	 
Group_level      char(12) NOT NULL,	
Family_level     char(12) NOT NULL,   
Line_level       char(12) NOT NULL,	  
Division_level   char(12) NOT NULL)
TABLESPACE Dim;
		
CREATE TABLE TimeLevel(
Tid	        varchar(12) PRIMARY KEY NOT NULL,   			
year_level      Number(4)  NOT NULL,			
Quarter_level   varchar(6) NOT NULL, 		 
month_level	Number(2),		
week_level	Number(2),		
day_level       Number(2))
TABLESPACE Dim;
		
CREATE TABLE ChanLevel(
Base_level     char(12) PRIMARY KEY NOT NULL,	
all_level      char(12) )
TABLESPACE Dim;
		
CREATE TABLE CustLevel( // client //// la table de dimension//
Store_level   char(12) PRIMARY KEY NOT NULL,    
Retailer_level char(12) NOT NULL)
TABLESPACE Dim;

CREATE TABLE actvars (//table de fait//
Customer_level      char(12) NOT NULL,
Product_level       char(12) NOT NULL,
Channel_level       char(12) NOT NULL,
Time_level          varchar(12) NOT NULL,
UnitsSold           FLOAT  ,
DollarSales         FLOAT  ,
DollarCost          FLOAT  ,
FOREIGN KEY (Customer_level) REFERENCES Custlevel(store_level),
FOREIGN KEY (product_level)  REFERENCES Prodlevel(code_level),  
FOREIGN KEY (Channel_level)  REFERENCES Chanlevel(base_level), 
FOREIGN KEY (Time_level)  REFERENCES Timelevel(tid))
tablespace Fact;












